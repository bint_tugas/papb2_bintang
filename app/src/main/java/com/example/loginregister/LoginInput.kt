package com.example.loginregister

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_login_input.btn_1b
import kotlinx.android.synthetic.main.activity_login_input.img_8g
import kotlinx.android.synthetic.main.activity_login_input_register.img_8h
import kotlinx.android.synthetic.main.activity_main.btn_1

class LoginInput : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_input)

        btn_1b.setOnClickListener{
            startActivity(Intent(this, Homepage::class.java))
        }

        img_8g.setOnClickListener{
            startActivity(Intent(this, LoginInputRegister::class.java))
        }

    }
}