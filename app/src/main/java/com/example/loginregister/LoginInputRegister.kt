package com.example.loginregister

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_login_input_register.btn_1c
import kotlinx.android.synthetic.main.activity_login_input_register.img_8h
import kotlinx.android.synthetic.main.activity_login_input_register.img_9
import kotlinx.android.synthetic.main.activity_login_register.btn_1a

class LoginInputRegister : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_input_register)

        btn_1c.setOnClickListener{
            startActivity(Intent(this, LoginInput::class.java))
        }
        img_9.setOnClickListener{
            startActivity(Intent(this, LoginInput::class.java))
        }
        img_8h.setOnClickListener{
            startActivity(Intent(this, LoginInput::class.java))
        }
    }
}