package com.example.loginregister

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_login_register.btn_1a
import kotlinx.android.synthetic.main.activity_login_register.btn_1a
import kotlinx.android.synthetic.main.activity_login_register.btn_2a
import kotlinx.android.synthetic.main.activity_main.btn_1

class LoginRegister : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_register)

        btn_1a.setOnClickListener{
            startActivity(Intent(this, LoginInput::class.java))
        }

        btn_2a.setOnClickListener{
            startActivity(Intent(this, LoginInputRegister::class.java))
        }


    }
}